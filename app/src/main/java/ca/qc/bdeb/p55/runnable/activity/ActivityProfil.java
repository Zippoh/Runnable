package ca.qc.bdeb.p55.runnable.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import ca.qc.bdeb.p55.runnable.R;
import ca.qc.bdeb.p55.runnable.common.AppPrefs;
import ca.qc.bdeb.p55.runnable.util.ActivityStructure;
import ca.qc.bdeb.p55.runnable.util.InputFilterNombreMinMax;

/**
 * Activité profil utilisateur.
 *
 * @author Marc-André
 * @since 15/12/2015
 */
public class ActivityProfil extends AppCompatActivity implements ActivityStructure {
    private Toolbar toolbar;
    private EditText txtNom;
    private EditText txtTaille;
    private EditText txtPoids;
    private EditText txtAge;
    private RadioGroup rdiogrpSexes;
    private RadioButton rdioHomme;
    private RadioButton rdioFemme;

    private AppPrefs prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);
        findViewsById();
        init();
    }

    @Override
    public void findViewsById() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        txtNom = (EditText) findViewById(R.id.activity_profil_txt_nom);
        txtTaille = (EditText) findViewById(R.id.activity_profil_txt_taille);
        txtPoids = (EditText) findViewById(R.id.activity_profil_txt_poid);
        txtAge = (EditText) findViewById(R.id.activity_profil_txt_age);
        rdiogrpSexes = (RadioGroup) findViewById(R.id.activity_profil_rdiogrp_sexes);
        rdioHomme = (RadioButton) findViewById(R.id.activity_profil_rdio_homme);
        rdioFemme = (RadioButton) findViewById(R.id.activity_profil_rdio_femme);
    }

    @Override
    public void init() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.activity_profil_txt_title));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        prefs = new AppPrefs(this);
        txtPoids.setFilters(new InputFilter[]{new InputFilterNombreMinMax(1, 600)});
        txtTaille.setFilters(new InputFilter[]{new InputFilterNombreMinMax(1, 250)});
        txtAge.setFilters(new InputFilter[]{new InputFilterNombreMinMax(1, 130)});
    }

    @Override
    protected void onResume() {
        loadPrefs();
        super.onResume();
    }

    @Override
    protected void onPause() {
        savePrefs();
        super.onPause();
    }

    /**
     * Charge les préférences dans l'UI
     */
    private void loadPrefs() {
        txtNom.setText(prefs.getName());
        txtTaille.setText(prefs.getTaille().toString());
        txtPoids.setText(prefs.getPoids().toString());
        txtAge.setText(prefs.getAge().toString());
        rdiogrpSexes.check(prefs.isHomme() ? rdioHomme.getId() : rdioFemme.getId());
    }

    /**
     * Sauvegarde les préférences modifiées
     */
    private void savePrefs() {
        String nom = txtNom.getText().toString().trim();
        int taille = Integer.parseInt(txtTaille.getText().toString().trim());
        int poids = Integer.parseInt(txtPoids.getText().toString().trim());
        int age = Integer.parseInt(txtAge.getText().toString().trim());
        boolean isHomme = rdiogrpSexes.getCheckedRadioButtonId() == rdioHomme.getId();


        if (!prefs.getName().equals(nom) && !nom.isEmpty()) {
            prefs.setName(nom);
        }
        if (prefs.getTaille() != taille) {
            prefs.setTaille(taille);
        }
        if (prefs.getPoids() != poids) {
            prefs.setPoids(poids);
        }
        if (prefs.getAge() != age) {
            prefs.setAge(age);
        }
        if (prefs.isHomme() != isHomme) {
            prefs.setIsHomme(isHomme);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                finish();
                return super.onOptionsItemSelected(item);
        }
    }
}
