package ca.qc.bdeb.p55.runnable.service;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.util.Log;

import ca.qc.bdeb.p55.runnable.dto.PasQuotidienDTO;
import ca.qc.bdeb.p55.runnable.persistence.PasQuotidienDAO;

/**
 * @author Marc-André Dion
 * @since 16/12/2015
 */
public class ServiceManager {
    private final Context context;
    private ServiceConnection mConnection;

    public ServiceManager(Context context) {
        this.context = context;
    }

    /**
     * Lie le service à l'application
     */
    public void connecterService(Intent mServiceIntent) {
        mConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName className, IBinder service) {
                Log.d("Main", "Service Creer");
            }

            @Override
            public void onServiceDisconnected(ComponentName arg0) {
                Log.d("Main", "Service detruit");
            }
        };

        context.bindService(mServiceIntent, mConnection, Context.BIND_AUTO_CREATE);
    }

    /**
     * Ajoute les pas acculer dans la bd quand l'application ouvre
     */
    public void ajouterBdPasService() {
        SharedPreferences settings = context.getSharedPreferences(PodometreService.PREF_INFO, 0);
        SharedPreferences.Editor editor = settings.edit();

        float verifNbrPas = settings.getFloat(PodometreService.NBR_PAS + 0, -1);
        int nbrJour = settings.getInt(PodometreService.NBR_JOUR, -1);
        PasQuotidienDAO pasQuotidienDAO = new PasQuotidienDAO(context);
        PasQuotidienDTO pasQuotidienDTO = new PasQuotidienDTO();

        Log.d("Main", "verifPas" + verifNbrPas);
        if (verifNbrPas != -1) {
            for (int i = 0; i < nbrJour; i++) {
                Float nbrPas = settings.getFloat(PodometreService.NBR_PAS + i, -1);
                pasQuotidienDTO.setDate(settings.getString(PodometreService.DATE, "null"));
                pasQuotidienDTO.setPas(Math.round(nbrPas));
                pasQuotidienDAO.ajouterLigne(pasQuotidienDTO);
                editor.remove(PodometreService.NBR_PAS + i);
                Log.d("Main", "nbrPAs" + nbrPas);
            }
            editor.remove(PodometreService.NBR_JOUR);
            editor.commit();
        }
    }

    /**
     * verifie si un service "run" en background
     *
     * @param serviceClass service qui est verifier
     * @return true si le service run
     */
    public boolean isMyServiceRunning(Class<?> serviceClass) {
        Log.d("Main", serviceClass.getName());
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void doUnbindService() {
        context.unbindService(mConnection);
    }
}
