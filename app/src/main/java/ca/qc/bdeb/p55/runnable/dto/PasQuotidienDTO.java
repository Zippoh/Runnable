package ca.qc.bdeb.p55.runnable.dto;

import android.support.annotation.NonNull;

/**
 * @author Marc-André Dion
 * @since 30/11/2015
 */
public class PasQuotidienDTO extends Dto<PasQuotidienDTO> {

    private String date;
    private int pas;

    public PasQuotidienDTO() {
    }

    @Override
    public int compareTo(@NonNull PasQuotidienDTO autreDTO) {
        return 0;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getPas() {
        return pas;
    }

    public void setPas(int pas) {
        this.pas = pas;
    }
}
