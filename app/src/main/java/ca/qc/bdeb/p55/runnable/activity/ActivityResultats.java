package ca.qc.bdeb.p55.runnable.activity;

import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.AppBarLayout.OnOffsetChangedListener;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

import ca.qc.bdeb.p55.runnable.R;
import ca.qc.bdeb.p55.runnable.dto.ActiviteDTO;
import ca.qc.bdeb.p55.runnable.dto.LocalisationDTO;
import ca.qc.bdeb.p55.runnable.persistence.LocalisationDAO;
import ca.qc.bdeb.p55.runnable.util.ActivityStructure;
import ca.qc.bdeb.p55.runnable.util.TimeDateUtil;
import ca.qc.bdeb.p55.runnable.util.TouchBlockingSupportMapFragment;

public class ActivityResultats extends AppCompatActivity implements OnClickListener, ActivityStructure, OnOffsetChangedListener, OnMapReadyCallback, TouchBlockingSupportMapFragment.OnTouchListener {

    /**
     * Clé pour identifier l'activité envoyé au intent
     */
    public static final String DATA_KEY = "ACTIVITE_ID";

    private ActiviteDTO activite;
    private LocalisationDAO localisationDAO;
    private boolean showToolbarButton;

    // Ui components
    private FloatingActionButton fab;
    private Toolbar toolbar;
    private CollapsingToolbarLayout collapsingToolbar;
    private AppBarLayout appBarLayout;
    private TextView txtDate;
    private TextView txtHeure;
    private TextView txtTypeActivite;
    private TextView txtNbPas;
    private TextView txtDuree;
    private TextView txtDistance;
    private TextView txtDepenseEnergetique;
    private TextView txtVitesse;
    private TouchBlockingSupportMapFragment mapFragment;
    private NestedScrollView scrollView;
    private GoogleMap map;
    private LinearLayout pnlPas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultats);
        findViewsById();
        init();

        loadData();
    }

    @Override
    public void findViewsById() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        txtDate = (TextView) findViewById(R.id.activity_resultat_date);
        txtHeure = (TextView) findViewById(R.id.activity_resultat_heure);
        txtTypeActivite = (TextView) findViewById(R.id.activity_resultat_type_activite);
        txtNbPas = (TextView) findViewById(R.id.activity_resultat_nb_pas);
        txtDuree = (TextView) findViewById(R.id.activity_resultat_duree);
        txtDistance = (TextView) findViewById(R.id.activity_resultat_distance);
        txtDepenseEnergetique = (TextView) findViewById(R.id.activity_resultat_depense_energetique);
        txtVitesse = (TextView) findViewById(R.id.activity_resultat_vitesse);
        mapFragment = (TouchBlockingSupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        scrollView = (NestedScrollView) findViewById(R.id.activity_resultat_scrollview);
        pnlPas = (LinearLayout) findViewById(R.id.activity_resultat_pnl_nb_pas);
    }

    @Override
    public void init() {
        fab.setOnClickListener(this);
        setSupportActionBar(toolbar);
        appBarLayout.addOnOffsetChangedListener(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        localisationDAO = new LocalisationDAO(this);
        mapFragment.setListener(this);
    }

    /**
     * Charge l'activité reçu dans l'interface et charge la map pour qu'elle dessine le trajet
     */
    private void loadData() {
        activite = (ActiviteDTO) getIntent().getExtras().getSerializable(DATA_KEY);

        String type = activite.getType().equals("COURSE") ? getString(R.string.type_activite_course) : getString(R.string.type_activite_velo);

        if(type.equals(getString(R.string.type_activite_velo))){
            pnlPas.setVisibility(View.GONE);
        }

        txtDate.setText(TimeDateUtil.parseDate(this.getApplicationContext(), activite.getDateDebut()));
        txtHeure.setText(TimeDateUtil.parseTime(this.getApplicationContext(), activite.getDateDebut()));
        txtTypeActivite.setText(type);
        txtNbPas.setText(Integer.toString(activite.getPas()));
        txtDuree.setText(TimeDateUtil.parseMilliseconds(activite.getDuree()));
        txtDistance.setText(String.format(getString(R.string.unite_distance), activite.getDistance()));
        txtVitesse.setText(String.format(getString(R.string.unite_vitesse), activite.getVitesseMoyenne()));
        txtDepenseEnergetique.setText(String.format(getString(R.string.unite_calorie), activite.getCalories()));
        mapFragment.getMapAsync(this);
    }

    /**
     * Demarre un intent pour partager les résultats sur les medias sociaux
     */
    private void shareIntent() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
        intent.putExtra(android.content.Intent.EXTRA_TEXT, String.format(getString(R.string.activity_resultat_format_distance), activite.getDistance(), TimeDateUtil.parseMilliseconds(activite.getDuree())));
        startActivity(Intent.createChooser(intent, getString(R.string.activity_resultat_chooser_partage)));
    }

    @Override
    public void onTouch() {
        scrollView.requestDisallowInterceptTouchEvent(true);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        redrawMap();
        focusWholeMap();
    }

    /**
     * Utilise la fonction {@link #dessineTrajet(Location, Location)} pour dessine le trajet de l'activité
     */
    private void redrawMap() {
        LocalisationDAO localisationDAO = new LocalisationDAO(this);

        List<LocalisationDTO> pointsTrajet = localisationDAO.getLocalisationByActiviteID(activite.getId());
        if (pointsTrajet.size() > 0) {
            Location lastLocation = new Location("");
            lastLocation.setLatitude(pointsTrajet.get(0).getLatitude());
            lastLocation.setLongitude(pointsTrajet.get(0).getLongitude());
            if (pointsTrajet.size() > 1) {
                for (int i = 1; i < pointsTrajet.size(); i++) {
                    Location newLoc = new Location("");
                    newLoc.setLatitude(pointsTrajet.get(i).getLatitude());
                    newLoc.setLongitude(pointsTrajet.get(i).getLongitude());
                    dessineTrajet(newLoc, lastLocation);
                    lastLocation = newLoc;
                }
            }
        }
    }

    /**
     * Dessine une ligne entre lastLocation et newLocation.
     *
     * @param newLocation location actuel
     */
    private void dessineTrajet(Location newLocation, Location lastLocation) {
        LatLng origine = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
        LatLng destination = new LatLng(newLocation.getLatitude(), newLocation.getLongitude());

        PolylineOptions line = new PolylineOptions();
        line.add(origine, destination);
        line.width(5);
        line.color(Color.RED);

        map.addPolyline(line);
    }

    private void focusWholeMap() {
        List<LocalisationDTO> locs = localisationDAO.getAllByID(activite.getId());
        if(!locs.isEmpty()) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (LocalisationDTO loc : locs) {
                builder.include(new LatLng(loc.getLatitude(), loc.getLongitude()));
            }
            LatLngBounds bounds = builder.build();

            map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 600, 600, 30));
        }
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (collapsingToolbar.getHeight() + verticalOffset < 2 * ViewCompat.getMinimumHeight(collapsingToolbar)) {
            showToolbarButton = true;
            invalidateOptionsMenu();
        }
        else {
            if (showToolbarButton) {
                showToolbarButton = false;
                invalidateOptionsMenu();
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v == fab) {
            shareIntent();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_resultats, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (showToolbarButton) {
            menu.findItem(R.id.activity_resultat_mnu_partager).setVisible(true);
        }
        else {
            menu.findItem(R.id.activity_resultat_mnu_partager).setVisible(false);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.activity_resultat_mnu_partager:
                shareIntent();
                return true;
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
