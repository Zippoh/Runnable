package ca.qc.bdeb.p55.runnable.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Hippolyte Glaus on 26/11/15.
 *
 * Uitiliser par le service pour enregistrer dans une liste le nombre de pas journalier
 */
public class JourPasDTO implements Serializable {
    private Date date;
    private int nbrPasJournalier;

    public JourPasDTO(Date date, int nbrPasJournalier) {
        this.date = date;
        this.nbrPasJournalier = nbrPasJournalier;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getNbrPasJournalier() {
        return nbrPasJournalier;
    }

    public void setNbrPasJournalier(int nbrPasJournalier) {
        this.nbrPasJournalier = nbrPasJournalier;
    }
}
