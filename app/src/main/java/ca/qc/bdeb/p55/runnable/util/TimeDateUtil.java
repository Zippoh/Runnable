package ca.qc.bdeb.p55.runnable.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.format.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * @author Marc-André Dion
 * @since 06/12/2015
 */
public abstract class TimeDateUtil {
    private static final String DEFAULT_TIME_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss";
    private static final SimpleDateFormat defaultDateFormat = new SimpleDateFormat(DEFAULT_TIME_FORMAT_PATTERN);
    private static final SimpleTimeZone utcTimeZone = new SimpleTimeZone(0,"UTC");

    private static String parseString(@NonNull Context context, @NonNull String utcTimeToFormat, int flags) {
        try {
            long unixTime = defaultDateFormat.parse(utcTimeToFormat).getTime();
            long timeZoneOffset = TimeZone.getDefault().getOffset(unixTime);
            long localUnixTime = unixTime + timeZoneOffset;

            return DateUtils.formatDateTime(context, localUnixTime, flags);
        } catch (ParseException e) {
            return "";
        }
    }

    public static String parseTime(@NonNull Context context, @NonNull String utcTimeToFormat){
        int flags = 0;
        flags |= DateUtils.FORMAT_SHOW_TIME;

        return parseString(context,utcTimeToFormat,flags);
    }

    public static String parseMonthDay(@NonNull Context context, @NonNull String utcTimeToFormat){
        int flags = 0;
        flags |= DateUtils.FORMAT_SHOW_DATE;
        flags |= DateUtils.FORMAT_ABBREV_MONTH;

        return parseString(context,utcTimeToFormat,flags);
    }

    public static String parseDate(@NonNull Context context, @NonNull String utcTimeToFormat){
        int flags = 0;
        flags |= DateUtils.FORMAT_SHOW_DATE;
        flags |= DateUtils.FORMAT_ABBREV_MONTH;
        flags |= DateUtils.FORMAT_SHOW_YEAR;

        return parseString(context,utcTimeToFormat,flags);
    }

    public static String parseDateTime(@NonNull Context context, @NonNull String utcTimeToFormat){
        int flags = 0;
        flags |= DateUtils.FORMAT_SHOW_TIME;
        flags |= DateUtils.FORMAT_SHOW_DATE;
        flags |= DateUtils.FORMAT_ABBREV_MONTH;
        flags |= DateUtils.FORMAT_SHOW_YEAR;

        return parseString(context, utcTimeToFormat, flags);
    }

    public static String currentDateTimeUTC(){
        Date date = new Date();
        SimpleDateFormat iso8601Format = new SimpleDateFormat(DEFAULT_TIME_FORMAT_PATTERN);
        iso8601Format.setTimeZone(new SimpleTimeZone(0, "UTC"));
        return iso8601Format.format(date);
    }

    public static String parseMilliseconds(long millis){
        long heure = TimeUnit.MILLISECONDS.toHours(millis);
        millis -= TimeUnit.HOURS.toMillis(heure);
        long minute = TimeUnit.MILLISECONDS.toMinutes(millis);
        millis -= TimeUnit.MINUTES.toMillis(minute);
        long seconde = TimeUnit.MILLISECONDS.toSeconds(millis);


        return String.format("%02d:%02d:%02d",heure,minute,seconde);
    }


}
