package ca.qc.bdeb.p55.runnable.util;

/**
 * Crée une structure de base pour une activity.
 *
 * @author Marc-André
 * @since  13/11/2015
 */
public interface ActivityStructure {

    /**
     * Trouve les views par leurs ID
     */
    void findViewsById();

    /**
     * Effectue le necessaire au démarrage de l'interface
     */
    void init();
}