package ca.qc.bdeb.p55.runnable.activity.accueil.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.List;

import ca.qc.bdeb.p55.runnable.R;
import ca.qc.bdeb.p55.runnable.dto.ActiviteDTO;
import ca.qc.bdeb.p55.runnable.persistence.ActiviteDAO;
import ca.qc.bdeb.p55.runnable.util.TimeDateUtil;
import ca.qc.bdeb.p55.runnable.common.TypeActivite;

/**
 * Fragment qui affiche un graphique à bande montrant la distance effectuer lors de chaque activités
 * Permet d'afficher ces informations triée par type d'activité
 */
public class GraphiqueDistanceActivite extends Fragment {
    private BarChart mChart;
    List<ActiviteDTO> listActiviteDto;

    public GraphiqueDistanceActivite() {
    }

    private TypeActivite typeActivite;

    public static android.support.v4.app.Fragment newInstance() {
        GraphiqueDistanceActivite fragment = new GraphiqueDistanceActivite();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_graphique_distance_activite, container, false);
        mChart = (BarChart) view.findViewById(R.id.bar_chart_distance);

        typeActivite = TypeActivite.COURSE;
        mChart.setNoDataTextDescription(getString(R.string.erreur_Aucun_resultat));
        mChart.setDescription("");
        mChart.setNoDataText("");

        mChart.setDrawGridBackground(false);
        mChart.setDrawBarShadow(false);
        mChart.getAxisRight().setEnabled(false);

        bdData();
        ajouterData();

        XAxis xAxis = mChart.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setLabelRotationAngle(90);
        mChart.invalidate();
        return view;
    }

    /**
     * Prend les donnée de listActiviteDto pour les mettrent dans les le graphique
     * Fait également le traitement des données
     */
    private void ajouterData() {
        ArrayList<String> xVals = new ArrayList<String>();
        ArrayList<BarEntry> activite = new ArrayList<BarEntry>();
        int cmpt = 0;

        for (int i = 0; i < listActiviteDto.size(); i++) {
            if (listActiviteDto.get(i).getType().equals("COURSE") && typeActivite == TypeActivite.COURSE) {
                activite.add(new BarEntry(Math.round(listActiviteDto.get(i).getDistance()), cmpt));
                String date = TimeDateUtil.parseMonthDay(getContext(), listActiviteDto.get(i).getDateDebut());
                xVals.add(date);
                cmpt++;
            }
        }

        cmpt = 0;
        for (int i = 0; i < listActiviteDto.size(); i++) {
            if (listActiviteDto.get(i).getType().equals("VELO") && typeActivite == TypeActivite.VELO) {
                String date = TimeDateUtil.parseMonthDay(getContext(), listActiviteDto.get(i).getDateDebut());
                xVals.add(date);
                activite.add(new BarEntry(Math.round(listActiviteDto.get(i).getDistance()), cmpt));
                cmpt++;
            }
        }
        BarDataSet set1;
        if (typeActivite == TypeActivite.VELO) {
            set1 = new BarDataSet(activite, getString(R.string.fragment_distance_activite_distance_velo));
            set1.setColor(Color.rgb(0, 121, 107));
        }
        else {
            set1 = new BarDataSet(activite, getString(R.string.fragment_distance_activite_distance_course));
            set1.setColor(Color.rgb(197, 17, 98));
        }

        set1.setBarSpacePercent(35f);
        set1.setColor(R.color.GraphiqueBande2);
        ArrayList<BarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);
        BarData data = new BarData(xVals, dataSets);
        data.setValueTextSize(10f);
        mChart.setData(data);
        mChart.invalidate();
    }

    /**
     * Cherche dans la bd les informations néssésaire pour afficher le graphique
     */
    private void bdData() {
        ActiviteDAO activiteDAO = new ActiviteDAO(getActivity());
        listActiviteDto = activiteDAO.getAllItems();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.fragment_graphiqueaccueil_mnu_course:
                typeActivite = TypeActivite.COURSE;
                ajouterData();
                return true;
            case R.id.fragment_graphiqueaccueil_mnu_velo:
                typeActivite = TypeActivite.VELO;
                ajouterData();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
