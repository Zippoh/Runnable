package ca.qc.bdeb.p55.runnable.util;

import android.content.Context;

import ca.qc.bdeb.p55.runnable.common.AppConstants;
import ca.qc.bdeb.p55.runnable.common.AppPrefs;
import ca.qc.bdeb.p55.runnable.common.TypeActivite;

/**
 * @author Marc-André Dion
 * @since 16/12/2015
 */
public class CalculateurCalories {
    private static final double BIKE_MET = 8.0;
    private static final double RUN_MET = 7.5;

    double basalMetabolicRate;
    double metabolicEquivalent;
    double durerEnHeures;
    int taille;
    int poids;
    int age;

    public CalculateurCalories(Context context, TypeActivite type) {
        AppPrefs prefs = new AppPrefs(context);
        taille = prefs.getTaille();
        poids = prefs.getPoids();
        age = prefs.getAge();

        if (prefs.isHomme()) {
            basalMetabolicRate = (13.75 * poids) + (5 * taille) - (6.76 * age) + 66;
        }
        else {
            basalMetabolicRate = (9.56 * poids) + (1.85 * taille) - (4.68 * age) + 655;
        }

        switch (type) {
            case VELO:
                metabolicEquivalent = BIKE_MET;
                break;
            case COURSE:
                metabolicEquivalent = RUN_MET;
                break;
        }
    }

    public double getCalorieBrulee(long millis){
        durerEnHeures = millis / AppConstants.MILLIS_TO_HOURS;
        return (basalMetabolicRate / 24)*metabolicEquivalent*durerEnHeures;
    }
}
