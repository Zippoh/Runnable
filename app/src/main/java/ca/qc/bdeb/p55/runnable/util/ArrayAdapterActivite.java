package ca.qc.bdeb.p55.runnable.util;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ca.qc.bdeb.p55.runnable.R;
import ca.qc.bdeb.p55.runnable.dto.ActiviteDTO;

/**
 * Adapter pour la ListView de client
 *
 * @author Marc-André Dion
 * @since 23/09/2015
 */
public class ArrayAdapterActivite extends ArrayAdapter<ActiviteDTO> {
    private final Context context;

    /**
     * Associe une vue avec l'adapteur et la ListView
     *
     * @param context
     * @param items
     */
    public ArrayAdapterActivite(Context context, List<ActiviteDTO> items) {
        super(context, R.layout.adapter_listview_activite, items);
        super.notifyDataSetChanged();
        this.context = context;
    }

    /**
     * Vide l'adapteur et le regenere avec les nouvelles données
     *
     * @param activiteDTOs liste d activite
     */
    public void refillAdapter(List<ActiviteDTO> activiteDTOs) {
        super.clear();
        super.addAll(activiteDTOs);
        super.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ActiviteDTO activiteDTO = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        LigneClient ligne;
        // Si le conteneur de ligne est neuf on le gonfle, sinon on reutilise le conteneur
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.adapter_listview_activite, null);
            ligne = new LigneClient();
            ligne.txtDate = (TextView) convertView.findViewById(R.id.array_adapter_contact_lbl_date);
            ligne.txtDuree = (TextView) convertView.findViewById(R.id.array_adapter_contact_lbl_duree);
            ligne.txtDistance = (TextView) convertView.findViewById(R.id.array_adapter_contact_lbl_distance);
            convertView.setTag(ligne);
        }
        else {
            ligne = (LigneClient) convertView.getTag();
        }

        // On ajoute les données
        ligne.txtDate.setText(TimeDateUtil.parseDateTime(context, activiteDTO.getDateDebut()));
        ligne.txtDuree.setText(TimeDateUtil.parseMilliseconds(activiteDTO.getDuree()));
        ligne.txtDistance.setText(String.format(context.getString(R.string.unite_distance), activiteDTO.getDistance()));
        return convertView;
    }

    /**
     * Classe qui contient les elements d'un ligne
     */
    private class LigneClient {
        private TextView txtDate;
        private TextView txtDuree;
        private TextView txtDistance;

    }
}
