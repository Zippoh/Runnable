package ca.qc.bdeb.p55.runnable.dto;

import android.support.annotation.NonNull;

/**
 * @author Marc-André Dion
 * @since 26/11/2015
 */
public class LocalisationDTO extends Dto<LocalisationDTO> {

    private long activiteID;
    private String timestamp;
    private double latitude;
    private double longitude;

    public LocalisationDTO(){

    }

    @Override
    public int compareTo(@NonNull LocalisationDTO autreDTO) {
        return 0;
    }

    public long getActiviteID() {
        return activiteID;
    }

    public void setActiviteID(long activiteID) {
        this.activiteID = activiteID;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
