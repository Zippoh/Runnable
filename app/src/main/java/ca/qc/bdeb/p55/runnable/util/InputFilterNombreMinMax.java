package ca.qc.bdeb.p55.runnable.util;

import android.text.InputFilter;
import android.text.Spanned;

/**
 * Empeche l'utilisateur de faire quoi que se soit sauf entrer des chiffre dans le range définit
 *
 * @author !Marc-André Dion
 * @since 23/09/2015
 * <p/>
 * Totalement emprunté de:
 * http://stackoverflow.com/questions/14212518/is-there-any-way-to-define-a-min-and-max-value-for-edittext-in-android
 * Sauf pour l'horrible ternaire...
 */
public class InputFilterNombreMinMax implements InputFilter {
    private final int min;
    private final int max;

    /**
     * Cree le filtre selon le range désiré
     *
     * @param min nombre minimum inclusivement
     * @param max nombre maximum inclusivement
     */
    public InputFilterNombreMinMax(int min, int max) {
        this.min = min;
        this.max = max;
    }

    /**
     * Recoit un changement a effectuer et determine si le changement est envoyé ou annulé
     *
     * @param source texte remplacement
     * @param start  pos debut du remplacement
     * @param end    pos fin du remplacement
     * @param dest   texte original
     * @param dstart pos debut a remplacer
     * @param dend   pos fin a remplacer
     * @return un string vide pour annuler l'entrée ou null pour valider...wow
     */
    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        // Extrait la portion à remplacer du string
        String chaine = dest.toString().substring(0, dstart) + dest.toString().substring(dend, dest.toString().length());
        // Ajoute la nouvelle portion au string
        chaine = chaine.substring(0, dstart) + source.toString() + chaine.substring(dstart, chaine.length());

        try {
            // On transfer la chaine en int
            int entree = Integer.parseInt(chaine);
            // Si le nouveau nombre est dans le range on accepte sinon on rejete
            if (isInRange(min, max, entree)) {
                return null;
            }
        } catch (NumberFormatException nfe) {
            return ""; // Si la nouvelle chaine n'est pas un int on rejete
        }

        return "";
    }

    /**
     * Verifie si un nombre est entre deux
     *
     * @param min    le nombre minimum inclusivement
     * @param max    le nombre maximum inclusivement
     * @param nombre le nombre a verifier
     * @return vrai si le nombre est dans le range
     */
    private boolean isInRange(int min, int max, int nombre) {
        if (max > min) {
            return nombre >= min && nombre <= max;
        }
        else {
            return nombre >= max && nombre <= min;
        }
    }
}
