package ca.qc.bdeb.p55.runnable.util;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ca.qc.bdeb.p55.runnable.R;
import ca.qc.bdeb.p55.runnable.activity.accueil.fragment.GraphiqueCourseVsVelo;
import ca.qc.bdeb.p55.runnable.activity.accueil.fragment.GraphiqueDistanceActivite;
import ca.qc.bdeb.p55.runnable.activity.accueil.fragment.GraphiqueTempsActivite;

/**
 * Adapter de fragment pour les graphiques
 *
 * @author Hippolyte
 * @since 09/12/15
 */
public class GraphiqueFragmentPagerAdapter extends FragmentPagerAdapter {
    private String[] tabTitles;
    private Fragment[] frags;

    public GraphiqueFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);

        tabTitles = new String[]{context.getString(R.string.type_graphique_temps),
                context.getString(R.string.type_graphique_distance),
                context.getString(R.string.type_graphique_type)};
        frags = new Fragment[] {GraphiqueTempsActivite.newInstance(),GraphiqueDistanceActivite.newInstance(),GraphiqueCourseVsVelo.newInstance()};
    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }

    @Override
    public Fragment getItem(int position) {
        return frags[position];
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}