package ca.qc.bdeb.p55.runnable.dto;

import android.support.annotation.NonNull;

import java.io.Serializable;

import ca.qc.bdeb.p55.runnable.util.TimeDateUtil;

/**
 * Objet qui contient les données d'une activité
 *
 * @author Marc-André
 * @since 17-09-2015
 */
public class ActiviteDTO extends Dto<ActiviteDTO> implements Serializable{

    private String type;
    private String dateDebut;
    private long duree;
    private double distanceTotale;
    private int pas;
    private double vitesseMoyenne;
    private double calories;

    /**
     * Contructeur qui crée un client vide
     */
    public ActiviteDTO() {
        this.type = "";
        this.setDateDebut(TimeDateUtil.currentDateTimeUTC());
        this.setDuree(0);
        this.setDistance(0);
        this.setPas(0);
        this.setVitesseMoyenne(0);
    }

    @Override
    public int compareTo(@NonNull ActiviteDTO anotherDTO) {
        return getType().compareTo(anotherDTO.getType());
    }

    /**
     * @return le type de famille
     */
    public String getType() {
        return type;
    }

    /**
     * @param type le type de famille
     */
    public void setType(@NonNull String type) {
        this.type = type;
    }

    public String getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(@NonNull String dateDebut) {
        this.dateDebut = dateDebut;
    }

    public double getDistance() {
        return distanceTotale;
    }

    public void setDistance(double distanceTotale) {
        this.distanceTotale = distanceTotale;
    }

    public long getDuree() {
        return duree;
    }

    public void setDuree(long duree) {
        this.duree = duree;
    }

    public int getPas() {
        return pas;
    }

    public void setPas(int pas) {
        this.pas = pas;
    }

    public void setPas(float pas) {
        this.pas = (int) pas;
    }

    public double getVitesseMoyenne() {
        return vitesseMoyenne;
    }

    public void setVitesseMoyenne(double vitesseMoyenne) {
        this.vitesseMoyenne = vitesseMoyenne;
    }

    public double getCalories() {
        return calories;
    }

    public void setCalories(double calories){
        this.calories = calories;
    }
}
