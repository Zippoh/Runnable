package ca.qc.bdeb.p55.runnable.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.List;

import ca.qc.bdeb.p55.runnable.dto.LocalisationDTO;

/**
 * @author Marc-André Dion
 * @since 26/11/2015
 */
public class LocalisationDAO extends Dao<LocalisationDTO> {

    private static final double MAX_LAT = 90;
    private static final double MAX_LONG = 180;
    private static final double MIN_LAT = -90;
    private static final double MIN_LONG = -180;

    public LocalisationDAO(Context context) {
        super(context, DBHelper.getLocalisationColonnes(), DBHelper.TABLE_LOCALISATION);
    }

    @Override
    ContentValues toContentValues(LocalisationDTO dto) {
        ContentValues ligne = new ContentValues();
        ligne.put(DBHelper.LOCALISATION_ACTIVITE_ID, dto.getActiviteID());
        ligne.put(DBHelper.LOCALISATION_TIMESTAMP, dto.getTimestamp());
        ligne.put(DBHelper.LOCALISATION_LATITUDE, dto.getLatitude());
        ligne.put(DBHelper.LOCALISATION_LONGITUDE, dto.getLongitude());
        return ligne;
    }

    @Override
    LocalisationDTO toDTO(Cursor pointeur) {
        LocalisationDTO dto = new LocalisationDTO();
        dto.setId(pointeur.getLong(0));
        dto.setActiviteID(pointeur.getLong(1));
        dto.setTimestamp(pointeur.getString(2));
        dto.setLatitude(pointeur.getDouble(3));
        dto.setLongitude(pointeur.getDouble(4));
        return dto;
    }

    public List<LocalisationDTO> getLocalisationByActiviteID(Long activiteID) {
        return getItemByCol(DBHelper.LOCALISATION_ACTIVITE_ID, activiteID.toString());
    }

    public List<LocalisationDTO> getAllByID(Long activiteID) {
        return getItemByCol(DBHelper.LOCALISATION_ACTIVITE_ID, activiteID.toString());

    }
}
