package ca.qc.bdeb.p55.runnable.activity.accueil.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.List;

import ca.qc.bdeb.p55.runnable.R;
import ca.qc.bdeb.p55.runnable.dto.PasQuotidienDTO;
import ca.qc.bdeb.p55.runnable.persistence.PasQuotidienDAO;

import static ca.qc.bdeb.p55.runnable.R.color.Graphic1Fill;
import static ca.qc.bdeb.p55.runnable.R.color.Graphic1Ligne;

/**
 * Affiche les statistiques de pas journaliers.
 *
 * @author Hippolyte
 * @since 06/12/2015
 */
public class PasJournaliers extends Fragment {
    private List<PasQuotidienDTO> dataList;
    private LineChart mChart;

    public PasJournaliers() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pasjournaliers, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.fragment_pasjournaliers));

        mChart = (LineChart) view.findViewById(R.id.chart);

        mChart.setNoDataTextDescription(getString(R.string.fragment_graphique_pas_quotidient_erreur_pas_journalier));
        mChart.setNoDataText("");
        mChart.setDescription(getString(R.string.fragment_graphique_pas_quotidient_description));
        mChart.setDrawGridBackground(false);

        getDataFromBd();
        if (dataList != null && dataList.size() > 0) {
            setData();
        }

        mChart.animateX(3000);

        mChart.getAxisRight().setEnabled(false);
        XAxis xAxis = mChart.getXAxis();
        xAxis.setEnabled(false);
        return view;
    }

    private void setData() {

        ArrayList<String> xVals = new ArrayList<String>();
        ArrayList<Entry> yVals = new ArrayList<Entry>();
        for (int i = 0; i < dataList.size(); i++) {
            xVals.add((i) + dataList.get(i).getDate());
            float val = (float) dataList.get(i).getPas();
            yVals.add(new Entry(val, i));
        }

        // create a dataset and give it a type
        LineDataSet set1 = new LineDataSet(yVals, getString(R.string.fragment_graphique_pas_quotidient));
        set1.setFillAlpha(30);

        set1.setColor(getResources().getColor(Graphic1Ligne));
        set1.setCircleColor(getResources().getColor(Graphic1Ligne));
        set1.setLineWidth(1f);
        set1.setCircleSize(3f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(9f);
        set1.setFillAlpha(80);
        set1.setFillColor(getResources().getColor(Graphic1Fill));
        set1.setDrawFilled(true);

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(set1); // add the datasets
        LineData data = new LineData(xVals, dataSets);
        mChart.setData(data);
    }

    private void getDataFromBd() {
        PasQuotidienDAO pasQuotidienDAO = new PasQuotidienDAO(getActivity());
        dataList = pasQuotidienDAO.getAllItems();
    }
}
