package ca.qc.bdeb.p55.runnable.activity.accueil;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import ca.qc.bdeb.p55.runnable.R;
import ca.qc.bdeb.p55.runnable.activity.ActivityHistorique;
import ca.qc.bdeb.p55.runnable.activity.ActivityProfil;
import ca.qc.bdeb.p55.runnable.activity.accueil.fragment.Accueil;
import ca.qc.bdeb.p55.runnable.activity.accueil.fragment.GraphiqueAccueil;
import ca.qc.bdeb.p55.runnable.activity.accueil.fragment.PasJournaliers;
import ca.qc.bdeb.p55.runnable.service.PodometreService;
import ca.qc.bdeb.p55.runnable.service.ServiceManager;

/**
 * Activity Accueil qui démarre le service et contient le drawer
 */
public class ActivityAccueil extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    Intent mServiceIntent;
    private ServiceManager serviceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        initService();
    }

    /**
     * Initialise et demarre le service
     */
    private void initService() {
        serviceManager = new ServiceManager(this);
        mServiceIntent = new Intent(this, PodometreService.class);
        if (!serviceManager.isMyServiceRunning(PodometreService.class)) {
            Log.d("Main", "service started");
            startService(mServiceIntent);
        }
        serviceManager.connecterService(mServiceIntent);
        serviceManager.ajouterBdPasService();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            Log.d("Main", "no backstack" + Integer.toString(getSupportFragmentManager().getBackStackEntryCount()));
            fragmentManager.popBackStack();
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_accueil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.fragment_accueil_mnu_profil) {
            Intent intent = new Intent(this, ActivityProfil.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        android.support.v4.app.Fragment fragment = null;
        getFragmentManager().beginTransaction();

        switch (id) {
            case R.id.nav_statistique_pas:
                fragment = new PasJournaliers();
                break;
            case R.id.nav_accueil:
                fragment = new Accueil();
                break;
            case R.id.nav_historique_course:
                Intent intent = new Intent(ActivityAccueil.this, ActivityHistorique.class);
                startActivity(intent);
                break;
            case R.id.nav_graphique_general:
                fragment = new GraphiqueAccueil();
                break;
        }

        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_accueil_placeholder, fragment)
                    .addToBackStack(null)
                    .commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        serviceManager.doUnbindService();
    }
}