package ca.qc.bdeb.p55.runnable.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import ca.qc.bdeb.p55.runnable.R;
import ca.qc.bdeb.p55.runnable.common.TypeActivite;
import ca.qc.bdeb.p55.runnable.dto.ActiviteDTO;
import ca.qc.bdeb.p55.runnable.persistence.ActiviteDAO;
import ca.qc.bdeb.p55.runnable.persistence.ActiviteDAO.Order;
import ca.qc.bdeb.p55.runnable.util.ArrayAdapterActivite;

/**
 * Activité qui liste l'historique des activité executé;
 */
public class ActivityHistorique extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private List<ActiviteDTO> activites;
    private ListView listViewActivite;
    private ArrayAdapterActivite adapterActivite;
    private ActiviteDAO activiteDAO;
    Order ordre;
    TypeActivite type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historique);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        listViewActivite = (ListView) findViewById(R.id.list_activite);

        ordre = Order.DATE_CROISSANTE;
        type = TypeActivite.VELO;

        activiteDAO = new ActiviteDAO(this);
        activites = activiteDAO.getOrderedAndFiltered(type, ordre);
        adapterActivite = new ArrayAdapterActivite(this, activites);
        listViewActivite.setAdapter(adapterActivite);
        listViewActivite.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(ActivityHistorique.this, ActivityResultats.class);
        adapterActivite.getItem(position);
        Bundle bundle = new Bundle();
        bundle.putSerializable(ActivityResultats.DATA_KEY, adapterActivite.getItem(position));
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.list_activite_date_decroissant:
                ordre = Order.DATE_DECROISSANT;
                item.setChecked(true);
                refreshListview();
                return true;
            case R.id.list_activite_date_croissant:
                ordre = Order.DATE_CROISSANTE;
                item.setChecked(true);
                refreshListview();
                return true;
            case R.id.list_activite_distance_crois:
                ordre = Order.DISTANCE_CROISSANTE;
                item.setChecked(true);
                refreshListview();
                return true;
            case R.id.list_activite_distance_decrois:
                ordre = Order.DISTANCE_DECROISSANTE;
                item.setChecked(true);
                refreshListview();
                return true;
            case R.id.mnu_velo:
                type = TypeActivite.VELO;
                refreshListview();
                invalidateOptionsMenu();
                return true;
            case R.id.mnu_course:
                type = TypeActivite.COURSE;
                refreshListview();
                invalidateOptionsMenu();
                return true;
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        switch (ordre){
            case DISTANCE_DECROISSANTE:
                menu.findItem(R.id.list_activite_distance_decrois).setChecked(true);
                break;
            case DATE_CROISSANTE:
                menu.findItem(R.id.list_activite_date_croissant).setChecked(true);
                break;
            case DISTANCE_CROISSANTE:
                menu.findItem(R.id.list_activite_distance_crois).setChecked(true);
                break;
            case DATE_DECROISSANT:
                menu.findItem(R.id.list_activite_date_decroissant).setChecked(true);
                break;
        }
        switch (type){
            case VELO:
                menu.findItem(R.id.mnu_course).setVisible(true);
                menu.findItem(R.id.mnu_velo).setVisible(false);
                break;
            case COURSE:
                menu.findItem(R.id.mnu_course).setVisible(false);
                menu.findItem(R.id.mnu_velo).setVisible(true);
                break;
        }
        return super.onPrepareOptionsMenu(menu);
    }

    /**
     * Update la liste de client, l'adapteur de la liste et notify l'adapteur qu'il a changé
     */
    private void refreshListview() {
        activites = activiteDAO.getOrderedAndFiltered(type, ordre);
        adapterActivite.refillAdapter(activites);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshListview();
    }
}
