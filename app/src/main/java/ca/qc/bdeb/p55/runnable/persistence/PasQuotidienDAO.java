package ca.qc.bdeb.p55.runnable.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import ca.qc.bdeb.p55.runnable.dto.PasQuotidienDTO;

/**
 * @author Marc-André Dion
 * @since 30/11/2015
 */
public class PasQuotidienDAO extends Dao<PasQuotidienDTO> {
    public PasQuotidienDAO(Context context){
        super(context, DBHelper.getPasQuotidienColonnes(), DBHelper.TABLE_PASQUOTIDIEN);
    }

    @Override
    ContentValues toContentValues(PasQuotidienDTO dto) {
        ContentValues ligne = new ContentValues();
        ligne.put(DBHelper.PASQUOTIDIENS_DATE, dto.getDate());
        ligne.put(DBHelper.PASQUOTIDIENS_PAS, dto.getPas());
        return ligne;
    }

    @Override
    PasQuotidienDTO toDTO(Cursor pointeur) {
        PasQuotidienDTO dto = new PasQuotidienDTO();
        dto.setId(pointeur.getLong(0));
        dto.setDate(pointeur.getString(1));
        dto.setPas(pointeur.getInt(2));
        return dto;
    }
}
