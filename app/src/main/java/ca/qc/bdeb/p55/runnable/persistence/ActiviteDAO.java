package ca.qc.bdeb.p55.runnable.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import ca.qc.bdeb.p55.runnable.dto.ActiviteDTO;
import ca.qc.bdeb.p55.runnable.common.TypeActivite;

/**
 * Interagit avec la base de donnée pour faire les ajouts,m-a-j et suppressions de données de la table AMI
 *
 * @author Marc-André Dion
 * @since 24/09/2015
 */
public class ActiviteDAO extends Dao<ActiviteDTO> {
    public enum Order {
        DISTANCE_DECROISSANTE, DATE_CROISSANTE, DISTANCE_CROISSANTE, DATE_DECROISSANT
    }

    /**
     * Demande l'instance de DBHelper.
     *
     * @param context
     */
    public ActiviteDAO(Context context) {
        super(context, DBHelper.getActiviteColonnes(), DBHelper.TABLE_ACTIVITE);
    }

    @Override
    ContentValues toContentValues(ActiviteDTO dto) {
        ContentValues ligne = new ContentValues();
        ligne.put(DBHelper.ACTIVITE_TYPE, dto.getType());
        ligne.put(DBHelper.ACTIVITE_DATE_DEBUT, dto.getDateDebut());
        ligne.put(DBHelper.ACTIVITE_DUREE, dto.getDuree());
        ligne.put(DBHelper.ACTIVITE_DISTANCE_TOTALE, dto.getDistance());
        ligne.put(DBHelper.ACTIVITE_PAS, dto.getPas());
        ligne.put(DBHelper.ACTIVITE_VITESSE_MOYENNE, dto.getVitesseMoyenne());
        ligne.put(DBHelper.ACTIVITE_CALORIES, dto.getCalories());
        return ligne;
    }

    @Override
    ActiviteDTO toDTO(Cursor pointeur) {
        ActiviteDTO dto = new ActiviteDTO();
        dto.setId(pointeur.getLong(0));
        dto.setType(pointeur.getString(1));
        dto.setDateDebut(pointeur.getString(2));
        dto.setDuree(pointeur.getLong(3));
        dto.setDistance(pointeur.getDouble(4));
        dto.setPas(pointeur.getInt(5));
        dto.setVitesseMoyenne(pointeur.getDouble(6));
        dto.setCalories(pointeur.getDouble(7));

        return dto;
    }

    public List<ActiviteDTO> getOrderedAndFiltered(TypeActivite type, Order order) {
        String colType = null;
        if(type != null){
            colType = type == TypeActivite.COURSE ? "\'COURSE\'" : "\'VELO\'";
        }
        switch (order) {
            case DATE_CROISSANTE:
                return getAllItemsBy(DBHelper.ACTIVITE_TYPE, colType, true, DBHelper.ACTIVITE_DATE_DEBUT);
            case DISTANCE_CROISSANTE:
                return getAllItemsBy(DBHelper.ACTIVITE_TYPE, colType, true, DBHelper.ACTIVITE_DISTANCE_TOTALE);
            case DATE_DECROISSANT:
                return getAllItemsBy(DBHelper.ACTIVITE_TYPE, colType, false, DBHelper.ACTIVITE_DATE_DEBUT);
            case DISTANCE_DECROISSANTE:
                return getAllItemsBy(DBHelper.ACTIVITE_TYPE, colType, false, DBHelper.ACTIVITE_DISTANCE_TOTALE);
        }
        return new ArrayList<>();
    }
}
