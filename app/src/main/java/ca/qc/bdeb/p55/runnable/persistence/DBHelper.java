package ca.qc.bdeb.p55.runnable.persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * S'occupe de crée et d'obtenir la BD
 *
 * @author Marc-André Dion
 * @since 22/10/2015
 */
class DBHelper extends SQLiteOpenHelper {
    // Nom BD
    private static final String DB_NOM = "runnable.db";

    // Version BD
    private static final int DB_VERSION = 1;

    // Instance unique de DBHelper
    private static DBHelper instance = null;

    // Tables
    static final String TABLE_ACTIVITE = "activite";
    static final String TABLE_LOCALISATION = "localisation";
    static final String TABLE_PASQUOTIDIEN = "pas_quotidien";

    // Colonne universelle ID
    static final String _ID = "_id";


    // Colonnes Table Activite
    static final String ACTIVITE_TYPE = "type";
    static final String ACTIVITE_DATE_DEBUT = "date_debut";
    static final String ACTIVITE_DUREE = "duree";
    static final String ACTIVITE_DISTANCE_TOTALE = "distance_totale";
    static final String ACTIVITE_PAS = "pas";
    static final String ACTIVITE_VITESSE_MOYENNE = "vitesse_moyenne";
    static final String ACTIVITE_CALORIES = "calories";

    //Colonnes Table Localisations
    static final String LOCALISATION_ACTIVITE_ID = "activite_id";
    static final String LOCALISATION_TIMESTAMP = "timestamp";
    static final String LOCALISATION_LATITUDE = "latitude";
    static final String LOCALISATION_LONGITUDE = "longitude";

    //Colonnes Table PasQuotidien
    public static final String PASQUOTIDIENS_DATE = "date";
    public static final String PASQUOTIDIENS_PAS = "pas";


    // Syntaxe SQLITE
    private static final String CREATE_TABLE_FORMAT = "CREATE TABLE %s (%s %s %s %s, ";
    private static final String TEXT_TYPE = " TEXT ";
    private static final String REAL_TYPE = " REAL ";
    private static final String INTEGER_TYPE = " INTEGER ";
    private static final String PK_CONSTRAINT = " PRIMARY KEY ";
    private static final String FK_CONSTRAINT_FORMAT = " FOREIGN KEY(%s) REFERENCES %s(%s) ";
    private static final String AUTOINCREMENT_CONSTRAINT = " AUTOINCREMENT ";
    private static final String NOT_NULL_CONSTRAINT = " NOT NULL ";
    public static final String COMMA = ", ";
    private static final String LINE_ENDING = ")";
    private static final String DROP_TABLE_FORMAT = " DROP TABLE IF EXISTS %s";


    // Requete SQLite pour creer la table Activite
    private final static String CREATE_TABLE_ACTIVITE = String.format(CREATE_TABLE_FORMAT,
            TABLE_ACTIVITE, _ID, INTEGER_TYPE, PK_CONSTRAINT, AUTOINCREMENT_CONSTRAINT)
            + ACTIVITE_TYPE + TEXT_TYPE + NOT_NULL_CONSTRAINT + COMMA
            + ACTIVITE_DATE_DEBUT + TEXT_TYPE + NOT_NULL_CONSTRAINT + COMMA
            + ACTIVITE_DUREE + INTEGER_TYPE + COMMA
            + ACTIVITE_DISTANCE_TOTALE + REAL_TYPE + COMMA
            + ACTIVITE_PAS + INTEGER_TYPE + COMMA
            + ACTIVITE_VITESSE_MOYENNE + REAL_TYPE + COMMA
            + ACTIVITE_CALORIES + REAL_TYPE
            + LINE_ENDING;

    // Requete SQLite pour creer la table Localisation
    private final static String CREATE_TABLE_LOCALISATION = String.format(CREATE_TABLE_FORMAT,
            TABLE_LOCALISATION, _ID, INTEGER_TYPE, PK_CONSTRAINT, AUTOINCREMENT_CONSTRAINT)
            + LOCALISATION_ACTIVITE_ID + INTEGER_TYPE + NOT_NULL_CONSTRAINT + COMMA
            + LOCALISATION_TIMESTAMP + TEXT_TYPE + NOT_NULL_CONSTRAINT + COMMA
            + LOCALISATION_LATITUDE + REAL_TYPE + NOT_NULL_CONSTRAINT + COMMA
            + LOCALISATION_LONGITUDE + REAL_TYPE + NOT_NULL_CONSTRAINT + COMMA
            + String.format(FK_CONSTRAINT_FORMAT, LOCALISATION_ACTIVITE_ID, TABLE_ACTIVITE, _ID)
            + LINE_ENDING;

    // Requete SQLite pour creer la table PasQuotidien
    private final static String CREATE_TABLE_PASQUOTIDIEN = String.format(CREATE_TABLE_FORMAT,
            TABLE_PASQUOTIDIEN, _ID, INTEGER_TYPE, PK_CONSTRAINT, AUTOINCREMENT_CONSTRAINT)
            + PASQUOTIDIENS_DATE + TEXT_TYPE + NOT_NULL_CONSTRAINT + COMMA
            + PASQUOTIDIENS_PAS + INTEGER_TYPE + NOT_NULL_CONSTRAINT
            + LINE_ENDING;


    /**
     * Renvoit l'instance de DBHelper.
     * Crée une instance de DBHelper si nécessaire
     *
     * @param contexte de la BD
     * @return L'instance de DBHelper
     */
    static DBHelper getInstance(Context contexte) {
        if (instance == null) {
            instance = new DBHelper(contexte.getApplicationContext());
        }
        return instance;
    }

    static String[] getActiviteColonnes(){
        return new String[]{_ID, ACTIVITE_TYPE, ACTIVITE_DATE_DEBUT, ACTIVITE_DUREE, ACTIVITE_DISTANCE_TOTALE, ACTIVITE_PAS, ACTIVITE_VITESSE_MOYENNE, ACTIVITE_CALORIES};
    }

    static String[] getLocalisationColonnes(){
        return new String[]{_ID, LOCALISATION_ACTIVITE_ID, LOCALISATION_TIMESTAMP, LOCALISATION_LATITUDE, LOCALISATION_LONGITUDE};
    }

    static String[] getPasQuotidienColonnes(){
        return new String[]{_ID, PASQUOTIDIENS_DATE, PASQUOTIDIENS_PAS};
    }

    // Constructeur privé qui crée un instance de DBHelper
    private DBHelper(Context contexte) {
        super(contexte, DB_NOM, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_ACTIVITE);
        db.execSQL(CREATE_TABLE_PASQUOTIDIEN);
        db.execSQL(CREATE_TABLE_LOCALISATION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(String.format(DROP_TABLE_FORMAT, TABLE_ACTIVITE));
        db.execSQL(String.format(DROP_TABLE_FORMAT, TABLE_LOCALISATION));
        db.execSQL(String.format(DROP_TABLE_FORMAT, TABLE_PASQUOTIDIEN));
        onCreate(db);
    }
}
