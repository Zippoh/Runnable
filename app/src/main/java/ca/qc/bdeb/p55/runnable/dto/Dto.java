package ca.qc.bdeb.p55.runnable.dto;

import android.support.annotation.NonNull;

import java.io.Serializable;

/**
 * @author Marc-André Dion
 * @since 23/10/2015
 */
abstract public class Dto<T extends Dto<T>> implements Comparable<T>, Serializable {
    public static final long DEFAULT_ID = -1;
    private long id;

    /**
     * Constructeur
     */
    Dto(){
        id = DEFAULT_ID;
    }

    public boolean sameId(@NonNull T autreDTO){
        return id != DEFAULT_ID && id == autreDTO.getId();
    }

    /**
     * @return id dans la BD
     */
    public long getId() {
        return id;
    }

    /**
     * @param id l'id dans la BD
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Retire les string nul
     *
     * @param texte texte d'entree
     * @return le texte d'entre ou un string vide s'il était null
     */
    String retirerNull(String texte) {
        if (texte == null) {
            texte = "";
        }
        else{
            texte = texte.trim();
        }
        return texte;
    }

    @Override
    public abstract int compareTo(@NonNull T autreDTO);
}
