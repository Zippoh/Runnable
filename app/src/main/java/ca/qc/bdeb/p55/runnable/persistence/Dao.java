package ca.qc.bdeb.p55.runnable.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import ca.qc.bdeb.p55.runnable.dto.Dto;

/**
 * Interagit avec la base de donnée pour faire les ajouts,m-a-j et suppressions de données
 *
 * @author Marc-André Dion
 * @since 23/10/2015
 */
abstract public class Dao<T extends Dto> {
    private static final String ASCENDING = " ASC";
    private static final String DESCEMDING = " DESC";
    static SQLiteDatabase db;
    final DBHelper dbHelper;
    final Context context;
    final String nomTable;
    String[] colonnes;

    public Dao(Context context, String[] colonnes, String nomTable) {
        dbHelper = DBHelper.getInstance(context);
        this.context = context;
        this.colonnes = colonnes;
        this.nomTable = nomTable;
    }

    /**
     * Ouvre ou obtient la connection ouverte à la DB
     */
    private void open() {
        db = dbHelper.getWritableDatabase();
    }

    /**
     * Ferme la connection à la DB si elle est ouverte
     */
    private void close() {
        dbHelper.close();
    }

    /**
     * Insere un nouveau dto dans la BD
     *
     * @param dto le dto a inseré
     * @return le dto inseré avec son id
     */
    public T ajouterLigne(T dto) {
        open();
        // Insere le dto dans la DB et recoit l'id de celui-ci
        long id = db.insert(nomTable, null, toContentValues(dto));

        // Select le dto avec l'id
        Cursor pointeur = db.query(nomTable, colonnes, DBHelper._ID + " = " + id, null, null, null, null);
        pointeur.moveToFirst();

        // Cree et retourne le AmiDTO avec le id
        dto = toDTO(pointeur);
        pointeur.close();
        close();
        return dto;
    }

    /**
     * Crée un ligne de ContentValues avec un Dto
     *
     * @param dto le dto a convertir
     * @return un ligne de ContentValue contenant le dto
     */
    abstract ContentValues toContentValues(T dto);

    /**
     * Convertit un pointeur de ligne en dto
     *
     * @param pointeur le curseur avec la ligne
     * @return un Dto
     */
    abstract T toDTO(Cursor pointeur);

    /**
     * Update un dto existant dans la BD
     *
     * @param dto le dto a modifier
     */
    public void modifierLigne(T dto) {
        open();
        db.update(nomTable, toContentValues(dto), DBHelper._ID + " = " + dto.getId(), null);
        close();
    }

    /**
     * Supprime un dto
     *
     * @param dto le dto a supprimer
     */
    public void supprimerLigne(T dto) {
        open();
        db.delete(nomTable, DBHelper._ID + " = " + dto.getId(), null);
        close();
    }

    /**
     * Assemble une liste de tout les clients dans la DB
     *
     * @return la liste de tout les clients
     */
    public List<T> getAllItems() {
        return getAllItemsBy(null, null, true, DBHelper._ID);
    }

    /**
     * Retourne l'entité correspondante au id
     *
     * @return la liste de tout les clients
     */
    public T getItemByID(Long id) {
        try {
            return getAllItemsBy(DBHelper._ID, id.toString(), true, DBHelper._ID).get(0);
        }
        catch (IndexOutOfBoundsException e){
            return null;
        }
    }

    /**
     * Retourne l'entité correspondante au id
     *
     * @return la liste de tout les clients
     */
    List<T> getItemByCol(@NonNull String matchCol, @NonNull String matchVal) {
        return getAllItemsBy(matchCol, matchVal, true, DBHelper._ID);
    }

    List<T> getAllItemsBy(String matchCol, String matchVal, boolean isAscendingOrder, String...cols) {

        String where = null;
        if (matchCol != null && matchVal != null) {
            where = matchCol + " = " + matchVal;
        }

        String orderBy = null;
        if (cols != null) {
            StringBuilder orderByBuilder = new StringBuilder();
            for (String col : cols) {
                if (orderByBuilder.length() > 0) {
                    orderByBuilder.append(DBHelper.COMMA);
                }
                orderByBuilder.append(col);
            }
            orderByBuilder.append(isAscendingOrder ? ASCENDING : DESCEMDING);
            orderBy = orderByBuilder.toString();
        }

        List<T> dtos = new ArrayList<>();
        open();
        // Rempli la liste
        Cursor pointeur = db.query(nomTable, colonnes, where, null, null, null, orderBy);
        pointeur.moveToFirst();
        while (!pointeur.isAfterLast()) {
            dtos.add(toDTO(pointeur));
            pointeur.moveToNext();
        }
        pointeur.close();
        close();
        return dtos;
    }

    List<T> getAllItemsOrderBy(boolean isAscendingOrder, @NonNull String... cols) {
        return getAllItemsBy(null, null, isAscendingOrder, cols);
    }
}
