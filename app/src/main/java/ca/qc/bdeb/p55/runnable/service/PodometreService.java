package ca.qc.bdeb.p55.runnable.service;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.SystemClock;
import android.util.Log;

import ca.qc.bdeb.p55.runnable.util.TimeDateUtil;

/**
 * Created by Hippolyte Glaus on 20/11/15.
 * <p/>
 * <p/>
 * Le service de podometre "run" sur un autre processus que ActivityAccueil, le service sauvegarde a chaque
 * 24h les pas dans un tableau. Lors de la prochaine ouverture de l'application "Runnable", l'application va caller
 * le service qui lui va lui envoyer la liste, par la suite la liste sera vidé.
 */

public class PodometreService extends Service implements SensorEventListener {

    public static final String PREF_INFO = "PREF_PAS";
    public static final String PREF_PAS_INITIAL = "PAS_INITIALE";
    public static final String NBR_JOUR = "NOMBRE_JOUR";
    public static final String NBR_PAS = "NOMBRE_PAS";
    public static final String DATE = "DATE";

    private boolean enregistrerPas;
    private boolean enregistrerPas1Fois;

    @Override
    public void onCreate() {
        super.onCreate();
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor compteurPas = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        sensorManager.registerListener(this, compteurPas, SensorManager.SENSOR_DELAY_UI);

        SharedPreferences settings = getSharedPreferences(PREF_INFO, Context.MODE_PRIVATE);
        float testPasInitial = settings.getFloat(PREF_PAS_INITIAL, -1);
        if (testPasInitial == -1) {
            timer24Heure();
            enregistrerPas = false;
            enregistrerPas1Fois = true;
        }
        else {
            enregistrerPas = false;
            enregistrerPas1Fois = false;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float nbrPas = event.values[0];
        if (enregistrerPas1Fois) {
            SharedPreferences settings = getSharedPreferences(PREF_INFO, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putFloat(PREF_PAS_INITIAL, nbrPas);
            editor.commit();
            enregistrerPas1Fois = false;
        }
        else if (enregistrerPas) {
            SharedPreferences settings = getSharedPreferences(PREF_INFO, 0);
            SharedPreferences.Editor editor = settings.edit();
            float nouvPAs2 = settings.getFloat(PREF_PAS_INITIAL, -1);
            int nbrJour = settings.getInt(NBR_JOUR, 0);
            float nouvPAs = nbrPas - nouvPAs2;
            editor.putFloat(NBR_PAS + nbrJour, nouvPAs);
            editor.putString(DATE, TimeDateUtil.currentDateTimeUTC());
            nbrJour++;
            editor.putInt(NBR_JOUR, nbrJour);
            editor.putFloat(PREF_PAS_INITIAL, nbrPas);
            editor.commit();
            enregistrerPas = false;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startForeground(startId, new Notification());
        return START_STICKY;
    }

    /**
     * A tout les 24 call la sous-methode onReceive
     */
    public void timer24Heure() {
        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent _) {

                if (!enregistrerPas1Fois) {
                    enregistrerPas = true;
                }
            }
        };

        this.registerReceiver(receiver, new IntentFilter("com.blah.blah.somemessage"));
        PendingIntent pintent = PendingIntent.getBroadcast(this, 0, new Intent("com.blah.blah.somemessage"), 0);
        AlarmManager manager = (AlarmManager) (this.getSystemService(Context.ALARM_SERVICE));
        manager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), 1000 * 60 * 60 * 24, pintent);
//        manager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), 1000 * 60, pintent);
    }

    static class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            Log.d("Service", "binder avecle msg");
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public IBinder onBind(Intent intent) {
        Messenger mMessenger = new Messenger(new IncomingHandler());
        return mMessenger.getBinder();
    }
}

