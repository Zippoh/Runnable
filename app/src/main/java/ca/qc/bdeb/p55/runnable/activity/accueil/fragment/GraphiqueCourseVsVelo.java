package ca.qc.bdeb.p55.runnable.activity.accueil.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

import ca.qc.bdeb.p55.runnable.R;
import ca.qc.bdeb.p55.runnable.persistence.ActiviteDAO;
import ca.qc.bdeb.p55.runnable.common.TypeActivite;

/**
 * Déssine un graphique circulaire montrant le nombre de fois ayant fait du vélo vs de la course
 */
public class GraphiqueCourseVsVelo extends Fragment {
    private PieChart mChart;

    public GraphiqueCourseVsVelo() {
    }

    public static android.support.v4.app.Fragment newInstance() {
        GraphiqueCourseVsVelo fragment = new GraphiqueCourseVsVelo();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_graphique_course_vs_velo, container, false);
        mChart = (PieChart) view.findViewById(R.id.graphique_course_vs_velo_pieChart);
        mChart.setDescription("");
        mChart.setNoDataText("");
        mChart.setNoDataTextDescription(getString(R.string.erreur_Aucun_resultat));

        mChart.setCenterTextSize(10f);
        mChart.setHoleRadius(45f);
        mChart.setTransparentCircleRadius(50f);

        Legend l = mChart.getLegend();
        l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
        generatePieData();
        return view;
    }


    private List getDataBd(TypeActivite type) {
        ActiviteDAO activiteDAO = new ActiviteDAO(getActivity());
        return activiteDAO.getOrderedAndFiltered(type, ActiviteDAO.Order.DATE_CROISSANTE);
    }

    private void generatePieData() {
        List listVelo = getDataBd(TypeActivite.VELO);
        List listCourse = getDataBd(TypeActivite.COURSE);
        int count = 2;
        ArrayList<Entry> entries1 = new ArrayList<Entry>();
        ArrayList<String> xVals = new ArrayList<String>();

        xVals.add(getString(R.string.type_activite_course));
        xVals.add(getString(R.string.type_activite_velo));

        for (int i = 0; i < count; i++) {
            xVals.add("entry" + (i + 1));
        }

        entries1.add(new Entry(listCourse.size(), 1));
        entries1.add(new Entry(listVelo.size(), 2));

        PieDataSet ds1 = new PieDataSet(entries1, getString(R.string.fragment_graphique_course_vs_velo_ratio));
        ds1.setColors(ColorTemplate.COLORFUL_COLORS);
        ds1.setSliceSpace(2f);
        ds1.setValueTextColor(Color.WHITE);
        ds1.setValueTextSize(12f);

        PieData d = new PieData(xVals, ds1);

        mChart.setData(d);
        mChart.invalidate();
    }
}
