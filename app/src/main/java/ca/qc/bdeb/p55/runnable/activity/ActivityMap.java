package ca.qc.bdeb.p55.runnable.activity;

import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

import ca.qc.bdeb.p55.runnable.R;
import ca.qc.bdeb.p55.runnable.common.AppConstants;
import ca.qc.bdeb.p55.runnable.common.TypeActivite;
import ca.qc.bdeb.p55.runnable.dto.ActiviteDTO;
import ca.qc.bdeb.p55.runnable.dto.Dto;
import ca.qc.bdeb.p55.runnable.dto.LocalisationDTO;
import ca.qc.bdeb.p55.runnable.persistence.ActiviteDAO;
import ca.qc.bdeb.p55.runnable.persistence.LocalisationDAO;
import ca.qc.bdeb.p55.runnable.util.ActivityStructure;
import ca.qc.bdeb.p55.runnable.util.CalculateurCalories;
import ca.qc.bdeb.p55.runnable.util.TimeDateUtil;

public class ActivityMap extends AppCompatActivity implements SensorEventListener,
        ActivityStructure, ConnectionCallbacks, LocationListener, OnConnectionFailedListener, OnMapReadyCallback {

    /**
     * Clé pour la reception du mode dans le intent
     */
    public static final String MODE = "qc.ca.bdeb.p55.runnable.activity.MapActivity";

    // Valeur pour la map et ses mise a jour
    private static final int CAMERA_ZOOM = 17;
    private static final int MAP_UPDATE_INTERVAL = 500;

    // Valeurs possible pour le type d'activité
    private static final String VELO = "VELO";
    private static final String COURSE = "COURSE";

    // Valeurs de conversion
    private static final int METER_TO_KM = 1000;

    // Sauvegarde pour changement de config
    private static final String ACTIVITE_SAVE = "sauvegarde activité dto";
    private static final String LAST_CHRONO_SAVE = "sauvegarde chronometre";

    // Derniere localisation
    private Location lastLocation;

    // Chrono
    private long chronoTimeWhenStopped;

    // UI
    private TextView txtNbPas;
    private TextView txtVitesseMoyenne;
    private TextView txtDistance;
    private TextView txtCalories;
    private Toolbar toolbar;
    private Chronometer chronometer;
    private GoogleMap map;

    // Update de map
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;

    // Flags
    private boolean chronometreEnCour;
    private boolean locationResquestActive;
    private boolean isBikeMode;
    private boolean isStepSensorRegistered;
    private boolean isMapReady;
    private boolean redrawMap;
    private boolean isSaved;

    // Models
    private ActiviteDTO activiteDTO;
    private LocalisationDTO localisationDTO;
    private CalculateurCalories calcCal;

    // Persistance
    private ActiviteDAO activiteDAO;
    private LocalisationDAO localisationDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        findViewsById();
        init();
    }

    @Override
    protected void onStart() {
        super.onStart();
        saveActiviteDTO();
        txtDistance.setText(String.format(getString(R.string.unite_distance), activiteDTO.getDistance()));
        txtNbPas.setText(String.format(getString(R.string.activity_map_txt_pas), activiteDTO.getPas()));
        txtVitesseMoyenne.setText(String.format(getString(R.string.unite_vitesse), activiteDTO.getVitesseMoyenne()));
        txtCalories.setText(String.format(getString(R.string.unite_calorie), activiteDTO.getCalories()));
        initStepSensor();
        googleApiClient.connect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        toggleChrono();
        if (googleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (chronometreEnCour) {
            toggleChrono();
        }
        outState.putSerializable(ACTIVITE_SAVE, activiteDTO);
        outState.putLong(LAST_CHRONO_SAVE, chronoTimeWhenStopped);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        activiteDTO = (ActiviteDTO) savedInstanceState.getSerializable(ACTIVITE_SAVE);
        chronoTimeWhenStopped = savedInstanceState.getLong(LAST_CHRONO_SAVE);
        redrawMap = true;
        txtDistance.setText(String.format(getString(R.string.unite_distance), activiteDTO.getDistance()));
        txtCalories.setText(String.format(getString(R.string.unite_calorie), activiteDTO.getCalories()));
        txtNbPas.setText(String.format(getString(R.string.activity_map_txt_pas), activiteDTO.getPas()));
    }

    @Override
    protected void onStop() {
        if (googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        terminerActivite(false);
        super.onDestroy();
    }

    @Override
    public void findViewsById() {
        txtNbPas = (TextView) findViewById(R.id.activity_map_txt_podometre);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        chronometer = (Chronometer) findViewById(R.id.activity_map_txt_chronometre);
        txtDistance = (TextView) findViewById(R.id.activity_map_txt_distance);
        txtVitesseMoyenne = (TextView) findViewById(R.id.activity_map_txt_vitessemoyenne);
        txtCalories = (TextView) findViewById(R.id.activity_map_txt_calories);
        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
    }

    @Override
    public void init() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        locationResquestActive = false;
        activiteDTO = new ActiviteDTO();
        activiteDAO = new ActiviteDAO(this);
        localisationDTO = new LocalisationDTO();
        localisationDAO = new LocalisationDAO(this);
        lastLocation = null;
        isBikeMode = false;
        isStepSensorRegistered = false;
        redrawMap = false;
        isMapReady = false;
        activiteDTO.setDateDebut(TimeDateUtil.currentDateTimeUTC());
        chronometreEnCour = false;
        isSaved = true;

        createLocationRequest();
        loadIntentData();
        initGoogleApi();
    }

    /**
     * Cree une requete de localisation dans {@link #locationRequest}
     */
    private void createLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(MAP_UPDATE_INTERVAL);
        locationRequest.setFastestInterval(MAP_UPDATE_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Charge les donné reçu dans le intent
     */
    private void loadIntentData() {
        switch (getIntent().getExtras().getInt(MODE)) {
            case R.id.fragment_accueil_btn_velo:
                activiteDTO.setType(VELO);
                txtNbPas.setVisibility(View.GONE);
                isBikeMode = true;
                calcCal = new CalculateurCalories(this, TypeActivite.VELO);
                Log.d("Intent Data: ", "bike mode");
                break;
            case R.id.fragment_accueil_btn_course:
                activiteDTO.setType(COURSE);
                isBikeMode = false;
                calcCal = new CalculateurCalories(this, TypeActivite.COURSE);
                Log.d("Intent Data: ", "run mode");
                break;
        }
    }

    /**
     * Initialise un objet de connection pour l'api de google s'il n'est pas déja initialisé
     */
    private void initGoogleApi() {
        if (googleApiClient == null) {
            Log.d("Google api: ", "Creating");
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    /**
     * Procede à la mise-à-jour ou à la création de l'entré de l'activité dans la bd.
     */
    private void saveActiviteDTO() {
        if (activiteDTO.getId() == Dto.DEFAULT_ID) {
            activiteDTO = activiteDAO.ajouterLigne(activiteDTO);
        }
        else {
            activiteDAO.modifierLigne(activiteDTO);
        }
        isSaved = false;
    }

    /**
     * Enregistre l'activité comme écouteur pour le sensor de pas si l'activité n'est pas de type velo
     */
    private void initStepSensor() {
        int version = android.os.Build.VERSION.SDK_INT;
        Log.i("API Version: ", Integer.toString(version));
        if (!isBikeMode && !isStepSensorRegistered && version >= Build.VERSION_CODES.KITKAT) {
            Log.d("Sensor listener: ", "Registering");
            SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
            Sensor stepSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
            if (stepSensor != null) {
                sensorManager.registerListener(this, stepSensor, SensorManager.SENSOR_DELAY_UI);
                isStepSensorRegistered = true;
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        isMapReady = true;
        map.setMyLocationEnabled(true);
        if (redrawMap) {
            redrawMap();
        }
    }

    /**
     * Redessine la map completement
     */
    private void redrawMap() {
        Log.d("redrawMap:", "Activite id = " + activiteDTO.getId());
        List<LocalisationDTO> pointsTrajet = localisationDAO.getLocalisationByActiviteID(activiteDTO.getId());
        if (pointsTrajet.size() > 0) {
            lastLocation = new Location("");
            lastLocation.setLatitude(pointsTrajet.get(0).getLatitude());
            lastLocation.setLongitude(pointsTrajet.get(0).getLongitude());
            if (pointsTrajet.size() > 1) {
                for (int i = 1; i < pointsTrajet.size(); i++) {
                    Location newLoc = new Location("");
                    newLoc.setLatitude(pointsTrajet.get(i).getLatitude());
                    newLoc.setLongitude(pointsTrajet.get(i).getLongitude());
                    updateTrajet(newLoc);
                    lastLocation = newLoc;
                }
            }
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (chronometreEnCour) {
            activiteDTO.setPas(activiteDTO.getPas() + event.values[0]);
            txtNbPas.setText(String.format(getString(R.string.activity_map_txt_pas), activiteDTO.getPas()));
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public void onLocationChanged(Location location) {
        if ((this.lastLocation == null || validateLocation(location)) && isMapReady && chronometreEnCour) {
            saveLocalisation(location);
            updateCamera(location);
            if (lastLocation != null) {
                updateTrajet(location);
                calculMetrique(location);
            }
            this.lastLocation = location;
        }
    }

    /**
     * Valide que la localisation en parametre est en dehors du champs d'imprécision de {@see lastLocation#getAccuracy}.
     * Cette validation permet d'éviter de tracer un trajet dans la zone imprécise.
     *
     * @param newLocation la nouvelle localisation
     * @return vrai si l'emplacement est en dehors du champs d'imprécision
     */
    private boolean validateLocation(Location newLocation) {
        Float accuracy = newLocation.getAccuracy();
        Float distance = this.lastLocation.distanceTo(newLocation);
        return distance > accuracy;
    }

    /**
     * Sauvegarde le point dans la base de donnée
     *
     * @param newLocation la localisation a sauvegarder
     */
    private void saveLocalisation(Location newLocation) {
        localisationDTO.setActiviteID(activiteDTO.getId());
        localisationDTO.setLatitude(newLocation.getLatitude());
        localisationDTO.setLongitude(newLocation.getLongitude());
        localisationDTO.setTimestamp(Long.toString(getDuree()));
        localisationDAO.ajouterLigne(localisationDTO);
    }

    /**
     * Deplace la camera vers la localisation recu avec un animation
     *
     * @param newLocation la nouvelle localisation
     */
    private void updateCamera(Location newLocation) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(newLocation.getLatitude(), newLocation.getLongitude()))
                .zoom(CAMERA_ZOOM)
                .bearing(newLocation.getBearing())
                .tilt(0)
                .build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    /**
     * Dessine une ligne entre {@link #lastLocation} et le parametre recu.
     *
     * @param newLocation location actuel
     */
    private void updateTrajet(Location newLocation) {
        LatLng origine = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
        LatLng destination = new LatLng(newLocation.getLatitude(), newLocation.getLongitude());

        PolylineOptions line = new PolylineOptions();
        line.add(origine, destination);
        line.width(5);
        line.color(Color.RED);

        map.addPolyline(line);
    }

    /**
     * Ajoute la distance entre {@link #lastLocation} et le parametre recu à {@see activiteDTO#getDistance}.
     *
     * @param newLocation location actuel
     */
    private void calculMetrique(Location newLocation) {
        activiteDTO.setDistance(activiteDTO.getDistance() + lastLocation.distanceTo(newLocation) / METER_TO_KM);
        activiteDTO.setVitesseMoyenne(activiteDTO.getDistance() / (getDuree() / AppConstants.MILLIS_TO_HOURS));
        activiteDTO.setCalories(calcCal.getCalorieBrulee(getDuree()));
        txtVitesseMoyenne.setText(String.format(getString(R.string.unite_vitesse), activiteDTO.getVitesseMoyenne()));
        txtDistance.setText(String.format(getString(R.string.unite_distance), activiteDTO.getDistance()));
        txtCalories.setText(String.format(getString(R.string.unite_calorie), activiteDTO.getCalories()));
    }

    /**
     * @return le temps écoulé depuis le début de l'activité
     */
    private long getDuree() {
        return SystemClock.elapsedRealtime() - chronometer.getBase();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (chronometreEnCour) {
            menu.findItem(R.id.pause).setVisible(true);
            menu.findItem(R.id.play).setVisible(false);
        }
        else {
            menu.findItem(R.id.pause).setVisible(false);
            menu.findItem(R.id.play).setVisible(true);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.pause:
                toggleChrono();
                return true;
            case R.id.play:
                toggleChrono();
                return true;
            case R.id.stop:
                terminerActivite(true);
                return true;
            case android.R.id.home:
                terminerActivite(false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        terminerActivite(false);
    }

    /**
     * Arrete ou redémarre {@link #chronometer}.
     */
    private void toggleChrono() {
        if (chronometreEnCour) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            chronoTimeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
            chronometer.stop();
        }
        else {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            chronometer.setBase(SystemClock.elapsedRealtime() + chronoTimeWhenStopped);
            chronometer.start();
        }
        chronometreEnCour = !chronometreEnCour;
        invalidateOptionsMenu();
    }

    /**
     * Met-à-jour les dernieres données dans la bd, démarre l'activité resultat et quitte.
     */
    private void terminerActivite(boolean openResult) {
        if (!isSaved) {
            stopLocationUpdates();
            if (chronometreEnCour) {
                toggleChrono();
            }
            activiteDTO.setDuree(getDuree());
            saveActiviteDTO();
            isSaved = true;
            if (openResult) {
                afficherIntentResultat();
            }
        }
        finish();
    }

    /**
     * Demare l'activity {@see ActivityResultats} et lui envoit {@link #activiteDTO}
     */
    private void afficherIntentResultat() {
        Intent intent = new Intent(ActivityMap.this, ActivityResultats.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(ActivityResultats.DATA_KEY, activiteDTO);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        startLocationUpdates();
    }

    /**
     * Demarre les updates de localisation pour la map
     */
    private void startLocationUpdates() {
        if (!locationResquestActive) {
            locationResquestActive = true;
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("onConnectionSuspended", "Suspended");
        googleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (chronometreEnCour) {
            toggleChrono();
        }
        if (googleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    /**
     * Arrete les update de localisation pour la map
     */
    private void stopLocationUpdates() {
        if (locationResquestActive) {
            locationResquestActive = false;
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e("onConnectionFailed", "Failed");
    }
}
