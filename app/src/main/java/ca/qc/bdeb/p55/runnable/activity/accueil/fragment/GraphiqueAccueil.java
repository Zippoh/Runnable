package ca.qc.bdeb.p55.runnable.activity.accueil.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import ca.qc.bdeb.p55.runnable.R;
import ca.qc.bdeb.p55.runnable.util.GraphiqueFragmentPagerAdapter;

/**
 * View pager qui contient les différents graphiques des statistiques.
 */
public class GraphiqueAccueil extends Fragment implements ViewPager.OnPageChangeListener {

    private GraphiqueFragmentPagerAdapter graphiqueFragmentPagerAdapter;
    private ViewPager viewPager;

    public GraphiqueAccueil() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.sliding_tab_layout, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.fragment_graphiqueaccueil_title));
        graphiqueFragmentPagerAdapter = new GraphiqueFragmentPagerAdapter(getChildFragmentManager(), getActivity());

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        viewPager.setAdapter(graphiqueFragmentPagerAdapter);
        viewPager.addOnPageChangeListener(this);
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_graphique_generale, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        switch (viewPager.getCurrentItem()) {
            case 2:
                menu.findItem(R.id.fragment_graphiqueaccueil_mnu_course).setVisible(false);
                menu.findItem(R.id.fragment_graphiqueaccueil_mnu_velo).setVisible(false);
                break;
            default:
                menu.findItem(R.id.fragment_graphiqueaccueil_mnu_course).setVisible(true);
                menu.findItem(R.id.fragment_graphiqueaccueil_mnu_velo).setVisible(true);
                break;
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
