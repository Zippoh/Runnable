package ca.qc.bdeb.p55.runnable.common;

/**
 * @author Marc-André Dion
 * @since 16/12/2015
 */
public class AppConstants {
    public static final String KEY_USERNAME = "username";
    public static final String DEFAULT_USERNAME = "John Doe";
    public static final String KEY_TAILLE = "taille";
    public static final int DEFAULT_TAILLE = 170;
    public static final String KEY_POIDS = "poids";
    public static final int DEFAULT_POIDS = 160;
    public static final String KEY_AGE = "age";
    public static final int DEFAULT_AGE = 30;
    public static final String KEY_GENRE_HOMME = "estunhomme";
    public static final boolean DEFAULT_GENRE_HOMME = true;
    public static final double MILLIS_TO_HOURS = 3600000;
}
