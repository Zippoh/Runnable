package ca.qc.bdeb.p55.runnable.common;

import android.content.Context;
import android.content.SharedPreferences;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

/**
 * @author Marc-André Dion
 * @since 16/12/2015
 */
public class AppPrefs {
    SharedPreferences prefs;

    public AppPrefs(Context context) {
        prefs = getDefaultSharedPreferences(context);
    }

    public String getName() {
        return prefs.getString(AppConstants.KEY_USERNAME, AppConstants.DEFAULT_USERNAME);
    }

    public void setName(String name) {
        prefs.edit().putString(AppConstants.KEY_USERNAME, name).apply();
    }

    public Integer getTaille() {
        return prefs.getInt(AppConstants.KEY_TAILLE, AppConstants.DEFAULT_TAILLE);
    }

    public void setTaille(int taille) {
        prefs.edit().putInt(AppConstants.KEY_TAILLE, taille).apply();
    }

    public Integer getPoids() {
        return prefs.getInt(AppConstants.KEY_POIDS, AppConstants.DEFAULT_POIDS);
    }

    public void setPoids(int poids) {
        prefs.edit().putInt(AppConstants.KEY_POIDS, poids).apply();
    }


    public Integer getAge() {
        return prefs.getInt(AppConstants.KEY_AGE, AppConstants.DEFAULT_AGE);
    }

    public void setAge(int age) {
        prefs.edit().putInt(AppConstants.KEY_AGE, age).apply();
    }

    public boolean isHomme() {
        return prefs.getBoolean(AppConstants.KEY_GENRE_HOMME, AppConstants.DEFAULT_GENRE_HOMME);
    }

    public void setIsHomme(boolean isHomme) {
        prefs.edit().putBoolean(AppConstants.KEY_GENRE_HOMME, isHomme).apply();
    }
}
