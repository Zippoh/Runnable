package ca.qc.bdeb.p55.runnable.activity.accueil.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import ca.qc.bdeb.p55.runnable.R;
import ca.qc.bdeb.p55.runnable.activity.ActivityMap;


public class Accueil extends Fragment implements OnClickListener {
    private Button btnCourse;
    private Button btnVelo;
    private LinearLayout btnStatistiques;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_accueil, container, false);
        btnCourse = (Button) rootView.findViewById(R.id.fragment_accueil_btn_course);
        btnCourse.setOnClickListener(this);
        btnVelo = (Button) rootView.findViewById(R.id.fragment_accueil_btn_velo);
        btnVelo.setOnClickListener(this);
        btnStatistiques = (LinearLayout) rootView.findViewById(R.id.fragment_accueil_btn_statistique);
        btnStatistiques.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_accueil_btn_course:
                startActiviteIntent(v.getId());
                break;
            case R.id.fragment_accueil_btn_velo:
                startActiviteIntent(v.getId());
                break;
            case R.id.fragment_accueil_btn_statistique:
               Fragment fragment = new GraphiqueAccueil();
                this.getFragmentManager().beginTransaction()
                        .replace(R.id.fragment_accueil_placeholder, fragment, "1")
                        .addToBackStack(null)
                        .commit();
        }
    }

    private void startActiviteIntent(int id) {
        Intent intent = new Intent(getActivity(), ActivityMap.class);
        intent.putExtra(ActivityMap.MODE,id);
        startActivity(intent);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_accueil, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
//            case R.id.menu_course:
//                sampleFragmentPagerAdapter.getItem(1);
//                return true;
//            case R.id.menu_run:
//
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
