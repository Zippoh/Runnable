package ca.qc.bdeb.p55.runnable.activity.accueil.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.List;

import ca.qc.bdeb.p55.runnable.R;
import ca.qc.bdeb.p55.runnable.dto.ActiviteDTO;
import ca.qc.bdeb.p55.runnable.persistence.ActiviteDAO;
import ca.qc.bdeb.p55.runnable.util.TimeDateUtil;
import ca.qc.bdeb.p55.runnable.common.TypeActivite;

/**
 * Fragment qui affiche une graphique a bande montrant le temps effectuer lors de chaque activités
 * Permet d'afficher ces informations trier par type d'activité
 */
public class GraphiqueTempsActivite extends Fragment {

    private TypeActivite typeActivite;

    private static final int MILLISECONDE = 1000;
    private static final int MINUTE = 60;

    private BarChart mChart;
    private List<ActiviteDTO> listActiviteDto;

    public GraphiqueTempsActivite() {
        // Required empty public constructor
    }

    public static android.support.v4.app.Fragment newInstance() {
        GraphiqueTempsActivite fragment = new GraphiqueTempsActivite();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        typeActivite = TypeActivite.COURSE;
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_graphique_temps_activite, container, false);
        mChart = (BarChart) view.findViewById(R.id.bar_chart);
        mChart.clear();

        mChart.setNoDataTextDescription(getString(R.string.erreur_Aucun_resultat));
        mChart.setDescription("");
        mChart.setNoDataText("");

        mChart.setDrawGridBackground(false);
        mChart.setDrawBarShadow(false);

        bdData();
        ajouterData();
        Legend l = mChart.getLegend();
        l.setTextSize(8f);

        mChart.getAxisRight().setEnabled(false);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setLabelRotationAngle(90);

        mChart.getAxisRight().setEnabled(false);
        mChart.invalidate();
        return view;
    }

    /**
     * Prend les donnée de listActiviteDto pour les mettrent dans les le graphique
     * Fait également le traitement des données pour afficher le temps en minute ainsi que le
     * formatage de la date
     */
    private void ajouterData() {
        int cmpt;
        ArrayList<String> xVals = new ArrayList<>();
        for (int i = 0; i < listActiviteDto.size(); i++) {
            if (listActiviteDto.get(i).getType().equals("COURSE") && typeActivite == TypeActivite.COURSE) {
                String date = TimeDateUtil.parseMonthDay(getContext(), listActiviteDto.get(i).getDateDebut());
                xVals.add(date);
            } else if (listActiviteDto.get(i).getType().equals("VELO") && typeActivite == TypeActivite.VELO) {
                String date = TimeDateUtil.parseMonthDay(getContext(), listActiviteDto.get(i).getDateDebut());
                xVals.add(date);
            }
        }

        ArrayList<BarEntry> yVals1 = new ArrayList<>();
        cmpt = 0;
        for (int i = 0; i < listActiviteDto.size(); i++) {
            if (listActiviteDto.get(i).getType().equals("COURSE") && typeActivite == TypeActivite.COURSE) {
                float temps = (listActiviteDto.get(i).getDuree() / MILLISECONDE) / MINUTE;
                temps += (float) ((listActiviteDto.get(i).getDuree() / MILLISECONDE) % MINUTE) / 100;
                yVals1.add(new BarEntry(temps, cmpt));
                cmpt++;
            }
        }

        cmpt = 0;
        for (int i = 0; i < listActiviteDto.size(); i++) {
            if (listActiviteDto.get(i).getType().equals("VELO") && typeActivite == TypeActivite.VELO) {
                float temps = (listActiviteDto.get(i).getDuree() / MILLISECONDE) / MINUTE;
                temps += (float) ((listActiviteDto.get(i).getDuree() / MILLISECONDE) % MINUTE) / 100;
                yVals1.add(new BarEntry(temps, cmpt));
                cmpt++;
            }
        }

        BarDataSet set1;
        if (typeActivite == TypeActivite.VELO) {
            set1 = new BarDataSet(yVals1, getString(R.string.fragment_distance_activite_temps_velo));
            set1.setColor(Color.rgb(0, 121, 107));
        } else {
            set1 = new BarDataSet(yVals1, getString(R.string.fragment_distance_activite_temps_course));
            set1.setColor(Color.rgb(197, 17, 98));
        }

        set1.setBarSpacePercent(35f);
        ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
        dataSets.add(set1);
        BarData data = new BarData(xVals, dataSets);
        data.setValueTextSize(10f);
        mChart.setData(data);
        mChart.invalidate();
    }

    /**
     * Cherche dans la bd les informations néssésaire pour afficher le graphique
     */
    private void bdData() {
        ActiviteDAO activiteDAO = new ActiviteDAO(getActivity());
        listActiviteDto = activiteDAO.getAllItems();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.fragment_graphiqueaccueil_mnu_course:
                typeActivite = TypeActivite.COURSE;
                ajouterData();
                return true;
            case R.id.fragment_graphiqueaccueil_mnu_velo:
                typeActivite = TypeActivite.VELO;
                ajouterData();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
